import 'package:flutter/material.dart';

import '../app_colors.dart';


class CustomSearchTextField extends StatelessWidget {
  final TextEditingController textController;
  final Function() onTap;

  final bool obscureText;
  final bool autofocus;
  final bool isLoading;
  final String hintText;
  final TextInputType keyboardType;
  final bool readOnly;
  final FocusNode focusNode;
  final Color fillColor;

  CustomSearchTextField(
      {this.obscureText = false,
      required this.textController,
      this.autofocus = false,
      this.keyboardType = TextInputType.text,
      this.readOnly = false,
      required this.focusNode,
      required this.onTap,
      this.fillColor = AppColors.colorWhite,
      required this.isLoading,
      this.hintText = "Search"});

  Widget build(BuildContext _context) {
    return Container(
      decoration: BoxDecoration(borderRadius: BorderRadius.circular(10),border: Border.all()),

      child: TextFormField(
        readOnly: readOnly,
        focusNode: focusNode,
        cursorColor: Colors.black,
        keyboardType: keyboardType,
        obscureText: obscureText,
        controller: textController,
        decoration: InputDecoration(
          suffixIconConstraints: BoxConstraints(minHeight: 30, minWidth: 30),
          contentPadding: EdgeInsets.all(10),
          isDense: true,
          filled: true,
          fillColor: AppColors.colorWhite,
          suffixIcon: Padding(
            padding: const EdgeInsets.only(right: 15.0),
            child: IconButton(
              icon: Icon(
                Icons.search_outlined,
                size: 24,
                color: Colors.black,
              ),
              onPressed: isLoading ? null : onTap,
            ),
          ),
          hintText: hintText,
          // hintStyle: TextStyles.textStyleRegular,
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(10),
            borderSide:
                BorderSide(color: AppColors.colorWhite, width: 1),
          ),
          enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(10),
            borderSide:
                BorderSide(color: AppColors.colorWhite, width: 2),
          ),
          border: OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(10)),
              borderSide:
                  BorderSide(color: AppColors.colorWhite, width: 2)),
        ),
      ),
    );
  }
}
