
import 'package:drinks_app/features/view/drinks_list_view.dart';
import 'package:drinks_app/splash.dart';
import 'package:flutter/material.dart';

import 'utils.dart/screen_routes.dart';


class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
     
      home: SplashView(),
      routes: {
        ScreenRoutes.drinksList: (context) =>
            const SafeArea(child: const DrinkListView()),
       
      },
    );
  }
}
