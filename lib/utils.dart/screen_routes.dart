import 'package:drinks_app/features/view/drinks_details_view.dart';
import 'package:drinks_app/features/view/drinks_list_view.dart';

class ScreenRoutes {
  static const String drinksList = DrinkListView.routeName;
  static const String drinksDetailsView = DrinksDetailsView.routeName;

 


}
