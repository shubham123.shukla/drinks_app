import 'package:flutter/material.dart';
import 'dart:async';
import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';

import 'app.dart';

void main() {
  //--------------------Widget initialisation-----------------------------------//
  WidgetsFlutterBinding.ensureInitialized();
  runZoned(() {
    SystemChrome.setPreferredOrientations(
            [DeviceOrientation.portraitUp, DeviceOrientation.portraitDown])
        .then((_) {
      runApp(  App());
    });
  });
}