import 'package:drinks_app/features/drink_list_widget.dart';
import 'package:drinks_app/features/drinks_controller.dart';
import 'package:drinks_app/features/drinks_response.dart';
import 'package:drinks_app/features/drinks_service.dart';
import 'package:drinks_app/utils.dart/app_colors.dart';
import 'package:drinks_app/utils.dart/app_strings.dart';
import 'package:drinks_app/utils.dart/screen_routes.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../utils.dart/widgets/custom_textfield.dart';
import 'drinks_details_view.dart';

class DrinkListView extends StatefulWidget {
  static const String routeName = "/DrinkListView";

  const DrinkListView({Key? key}) : super(key: key);

  @override
  State<DrinkListView> createState() => _DrinkListViewState();
}

class _DrinkListViewState extends State<DrinkListView> {
  final DrinksController _drinksController = Get.put(DrinksController());
  // @override
  // void initState() {
  //   _drinksController.init();
  //   super.initState();
  // }

  // @override
  // void dispose() {
  //   _drinksController.dispose();
  //   super.dispose();
  // }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _buildDrinks(),
    );
  }

  _buildDrinks() {
    return GestureDetector(
      child: Obx(
        () => Column(
          children: [
            _buildSearchTextField(),
            (_drinksController.isLoading.value)
                ? const Center(child: const CircularProgressIndicator())
                : _buildSearchList()
          ],
        ),
      ),
    );
  }

  _buildSearchTextField() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 16),
      child: CustomSearchTextField(
        hintText: "AppString.search",
        textController: _drinksController.searchTextController,
        focusNode: _drinksController.focusNode,
        readOnly: _drinksController.isLoading.value ? true : false,
        onTap: _drinksController.ontapField,
        isLoading: _drinksController.isLoading.value,
      ),
    );
  }

  _buildSearchList() {
    return Expanded(
        child: ListView.builder(
            physics: const ClampingScrollPhysics(),
            shrinkWrap: true,
            itemCount: _drinksController.drinkList.length,
            itemBuilder: (BuildContext context, int index) {
              return _buildList(_drinksController.drinkList[index], index);
            }));
  }

  _buildList(Drinks model, int index) {
    return Container(
        margin: const EdgeInsets.all(10),
        decoration:
            BoxDecoration(border: Border.all(color: AppColors.colorWhite)),
        child: DrinksListWidget(
          title: model.strDrink ?? "Drink Title",
          subTitle: model.strCategory ?? "Category",
          imageUrl: model.strDrinkThumb ?? "",
          onTap: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) {
                  return DrinksDetailsView(
                    model: model,
                  );
                },
              ),
            );
          },
        ));
  }
}
