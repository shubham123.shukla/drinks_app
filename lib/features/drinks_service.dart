import 'package:dio/dio.dart';
import 'package:drinks_app/features/drinks_response.dart';

class DrinksService {
  final Dio dio = Dio();
  Future<DrinksResponse> getDrinksList({String? search}) async {
    var url =
        'https://www.thecocktaildb.com/api/json/v1/1/search.php?s=$search';
    var response = await dio.get(url);
    print('Response status: ${response.statusCode}');

    print("$response");
    if (response.statusCode == 200) {
      return DrinksResponse.fromJson(response.data);
    } else {
      throw Exception("Failed to get search drinks by name");
    }
  }
}
