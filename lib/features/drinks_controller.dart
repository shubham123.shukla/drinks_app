// import 'package:flutter/cupertino.dart';

import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:get/state_manager.dart';

import 'drinks_response.dart';
import 'drinks_service.dart';

class DrinksController extends GetxController {
  RxBool isLoading = false.obs;
  TextEditingController searchTextController = TextEditingController();
  DrinksService service = DrinksService();

  RxList<Drinks> _drinks = <Drinks>[].obs;
  ScrollController scrollController = ScrollController();
  int get drinksLength => _drinks.length;
  List<Drinks> get drinkList => _drinks;

  FocusNode focusNode = FocusNode();
  RxBool isInit = false.obs;

  getDrinksList() async {
    print("2");
    try {
      isLoading.value = true;
      var data = await DrinksService()
          .getDrinksList(search: searchTextController.text);
      if (data != null) {
        isLoading.value = false;
        print("3");
        _drinks.value = data.drinks ?? <Drinks>[].obs;
      }
    } catch (error) {
      isLoading.value = false;

      log(error.toString());
    }
  }

  // init() {
  //   // isInit.value = true;
  //   // searchTextController.addListener(_onSearchTextChanged);
  //   searchTextController.addListener(() {
  //     if (searchTextController.text.isEmpty && focusNode.hasFocus) {
  //       // isInit.value = true;

  //       drinkList.clear();
  //        ontapField();
  //     }
  //   });
  // }

  // dispose() {
  //   scrollController.removeListener(onScroll);
  //   scrollController.dispose();
  //   // searchTextController.removeListener(_onSearchTextChanged);
  //   searchTextController.dispose();
  // }

  ontapField() async {
    print("object");
    if (searchTextController.text.isEmpty) {
      return;
    }
    if (focusNode.hasFocus) {
      focusNode.unfocus();
    } else {
      focusNode.requestFocus();
    }
    if (searchTextController.text.trim().isNotEmpty) {
      if (isInit.value) {
        isInit.value = false;
      }
      await getDrinksList();
    }
  }
}
