import 'package:flutter/material.dart';


class DrinksListWidget extends StatelessWidget {
  final String title;
  final String subTitle;
  final String imageUrl;
  final Function() onTap;

  const DrinksListWidget(
      {Key? key,
      required this.title,
      required this.subTitle,
      required this.imageUrl,
      required this.onTap})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(borderRadius: BorderRadius.circular(10),border: Border.all()),
      child: ListTile(
        onTap: onTap,
        leading: Image.network(imageUrl),
        title: Text(title),
        subtitle: Text(subTitle),
        trailing: Container(
          margin: const EdgeInsets.all(16),
          width: 34,
          height: 34,
          child: const Icon(
            Icons.arrow_forward_ios,
            color: Colors.black,
            size: 15,
          ),
        ),
      ),
    );
  }
}
