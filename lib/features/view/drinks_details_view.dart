import 'package:drinks_app/features/drink_list_widget.dart';
import 'package:drinks_app/features/drinks_response.dart';
import 'package:flutter/material.dart';

class DrinksDetailsView extends StatelessWidget {
  static const String routeName = "/DrinksDetailsView";

  final Drinks model;
  const DrinksDetailsView({Key? key, required this.model}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    var mediaQuery = MediaQuery.of(context);
    return Scaffold(
      appBar: AppBar(),
        body: DrinksListWidget(
      title: model.strDrink ?? "Drink Title",
      subTitle: model.strCategory ?? "Category",
      imageUrl: model.strDrinkThumb ?? "",
      onTap: () {},
    ));
  }
}
