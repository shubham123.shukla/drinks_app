import 'dart:async';

import 'package:flutter/material.dart';

import 'utils.dart/app_strings.dart';
import 'utils.dart/screen_routes.dart';

class SplashView extends StatefulWidget {
  @override
  _SplashViewState createState() => _SplashViewState();
}

class _SplashViewState extends State<SplashView> {
  @override
  void initState() {
    super.initState();
    Timer(const Duration(seconds: 1), () {
      Navigator.pushReplacementNamed(context, ScreenRoutes.drinksList);
    });
  }

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      body: const Center(
        child: Text(
          AppString.text,
        ),
      ),
    );
  }
}
